# TP MongoDB: prise en main et pipeline d'agrégation - suite

Ce TP prend la suite du [tp mongo](tp2-mongo.md).

## 4. Pipelines d'agrégation

Dans cette partie, on va créer des pipelines d'aggrégation en enchaînant plusieurs opérateurs. Il faudra prévoir l'enchaînement des transformations à effectuer, et pour chaque transformation du pipeline, bien prévoir le type des documents du résultat afin de pourvoir configurer correctement les transformations suivantes.

**Exemple**: donner, par ordre de population décroissante, les états ayant plus de 10 000 000 d'habitants:

```mongodb
db.zips.aggregate([
    { $group: { _id: "$state",
                population: { $sum: "$pop" } } },
    { $match: { population: { $gte: 10000000 } } },
    { $sort: { population: -1 }},
])
```

Répondre aux questions suivantes via une requête utilisant un pipeline d'agrégation.

1. Calculer le nombre de notes de chaque type.
   _Indice:_ Commencer par extraire les notes individuellement avec `$unwind`.
   _Indice:_ On peut calculer un nombre d'occurrences en sommant la valeur `1` sur un groupe.
2. Donner pour chaque matière, la meilleure note d'examen.
3. Donner les dix villes les plus peuplées.
   _Indice:_ On pourra utiliser l'étape [$limit].
4. Donner la population moyenne des villes pour chaque état.
5. Donner pour chaque matière, les étudiants ayant une note d'examen supérieure à 50.
   _Indice:_ On pourra utiliser l'opérateur d'agrégation [\$push][$push] dans un [$group] pour constituer la liste.
6. Donner pour chaque étudiant et chaque matière sa note générale. La note générale est la moyenne de chaque type de note. S'il y a plusieurs notes d'un même type, on prendra d'abord la moyenne de ce type avant de l'utiliser pour calculer la moyenne avec les autres types.
   _Indice:_ Commencer par calculer pour chaque étudiant, chaque matière et chaque type de note sa moyenne.
7. Donner, pour chaque matière le type d'épreuve la mieux réussie. Le type d'épreuve la mieux réussie est celui ayant la meilleure moyenne, calculée sur l'ensemble des notes de ce type pour cette matière.
   _Indice:_ Un moyen de faire consiste à calculer la meilleure moyenne (via [\$max][$max]), tout en conservant l'ensemble des moyennes dans un tableau via [$push].

[mongodb-compass]: https://www.mongodb.com/products/compass
[mongodb-install]: https://docs.mongodb.com/manual/installation/#mongodb-installation-tutorials
[docker]: https://www.docker.com/get-started
[docker-linux]: https://docs.docker.com/engine/install/#server
[mif04-mongodb.zip]: https://perso.liris.cnrs.fr/emmanuel.coquery/enseignement/mif04/mif04-mongodb.zip
[findone]: https://docs.mongodb.com/manual/reference/method/db.collection.findOne/
[find]: https://docs.mongodb.com/manual/reference/method/db.collection.find/
[query-selectors]: https://docs.mongodb.com/manual/reference/operator/query/#query-selectors
[aggregation-expression]: https://docs.mongodb.com/manual/meta/aggregation-quick-reference/#std-label-aggregation-expressions
[aggregation-expression-operators]: https://docs.mongodb.com/manual/meta/aggregation-quick-reference/#operator-expressions
[projection]: https://docs.mongodb.com/manual/reference/method/db.collection.find/#std-label-find-projection
[aggregation-pipeline]: https://docs.mongodb.com/manual/aggregation/#std-label-aggregation-framework
[$project]: https://docs.mongodb.com/manual/reference/operator/aggregation/project/#mongodb-pipeline-pipe.-project
[$match]: https://docs.mongodb.com/manual/reference/operator/aggregation/match/#mongodb-pipeline-pipe.-match
[$sort]: https://docs.mongodb.com/manual/reference/operator/aggregation/sort/
[$group]: https://docs.mongodb.com/manual/reference/operator/aggregation/group/
[group-agg-operators]: https://docs.mongodb.com/manual/reference/operator/aggregation/group/#std-label-accumulators-group
[$lookup]: https://docs.mongodb.com/manual/reference/operator/aggregation/lookup/
[$limit]: https://docs.mongodb.com/manual/reference/operator/aggregation/limit/
[$max]: https://docs.mongodb.com/manual/reference/operator/aggregation/max/#mongodb-group-grp.-max
[$push]: https://docs.mongodb.com/manual/reference/operator/aggregation/push/#mongodb-group-grp.-push
